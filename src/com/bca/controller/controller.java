package com.bca.controller;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;

import org.apache.log4j.Logger;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.internal.LinkedTreeMap;
import com.google.gson.reflect.TypeToken;

import adapter.DeviceManagementAdapter;
import adapter.LogAdapter;
import model.Globals;

@RestController
public class controller {
	final static Logger logger = Logger.getLogger(controller.class);

	@RequestMapping(value = "/ping",method = RequestMethod.POST,consumes="application/json",headers="Accept=application/json")
	public @ResponseBody void GetPing()
	{
		return;
	}

    @RequestMapping(value = "/update",method = RequestMethod.POST,consumes="application/json",headers="Accept=application/json")
	public @ResponseBody model.mdlAPIResult UpdateDeviceManagement(@RequestBody model.mdlDeviceManagement param)
	{
		model.mdlAPIResult mdlUpdateDeviceManagementResult = new model.mdlAPIResult();
		model.mdlErrorSchema mdlErrorSchema = new model.mdlErrorSchema();
		model.mdlMessage mdlMessage = new model.mdlMessage();
		model.mdlResult mdlResult = new model.mdlResult();
		Gson gson = new Gson();

		model.mdlLog mdlLog = new model.mdlLog();
		mdlLog.WSID = param.WSID;
		mdlLog.SerialNumber = param.SerialNumber;
		mdlLog.ApiFunction = "updateDeviceManagement";
		mdlLog.SystemFunction = "UpdateDeviceManagement";
		mdlLog.LogSource = "Webservice";
		mdlLog.LogStatus = "Failed";

		mdlResult.Result = "false";

		Boolean checkDeviceManagement, checkBranchCode, isSuccess, checkWSIDBranchCode = false;
		Integer checkWSID = 1;

		try {
		    	model.mdlDeviceManagement previousDeviceData = DeviceManagementAdapter.GetDeviceData(param.SerialNumber, param.WSID);
		    	
			checkBranchCode = DeviceManagementAdapter.CheckBranchCode(param.BranchCode, param.BranchTypeID, param.BranchInitial, param.SerialNumber, param.WSID);
			if (!checkBranchCode){
				mdlErrorSchema.ErrorCode = "01";
				mdlMessage.Indonesian = "Kode Cabang Salah Input";
				mdlMessage.English = mdlLog.ErrorMessage = "Wrong Input Branch Code";
				mdlErrorSchema.ErrorMessage = mdlMessage;
				mdlUpdateDeviceManagementResult.ErrorSchema = mdlErrorSchema;
				mdlUpdateDeviceManagementResult.OutputSchema = mdlResult;
				LogAdapter.InsertLog(mdlLog);
				return mdlUpdateDeviceManagementResult;
			}

			checkWSIDBranchCode = DeviceManagementAdapter.CheckWSIDBranchCode(param.BranchCode, param.WSID, param.SerialNumber);
			if (!checkWSIDBranchCode){
				mdlErrorSchema.ErrorCode = "04";
				mdlMessage.Indonesian = "Kode Cabang Dan WSID Tidak Sama";
				mdlMessage.English = mdlLog.ErrorMessage = "Branch Code And WSID Not Matched";
				mdlErrorSchema.ErrorMessage = mdlMessage;
				mdlUpdateDeviceManagementResult.ErrorSchema = mdlErrorSchema;
				mdlUpdateDeviceManagementResult.OutputSchema = mdlResult;
				LogAdapter.InsertLog(mdlLog);
				return mdlUpdateDeviceManagementResult;
			}

			checkWSID = DeviceManagementAdapter.CheckWSID(param.SerialNumber, param.WSID);
			if (checkWSID == 1){
				mdlErrorSchema.ErrorCode = "05";
				mdlMessage.Indonesian = "WSID Sudah Ada";
				mdlMessage.English = mdlLog.ErrorMessage = "WSID Already Exists";
				mdlErrorSchema.ErrorMessage = mdlMessage;
				mdlUpdateDeviceManagementResult.ErrorSchema = mdlErrorSchema;
				mdlUpdateDeviceManagementResult.OutputSchema = mdlResult;
				LogAdapter.InsertLog(mdlLog);
				return mdlUpdateDeviceManagementResult;
			}else if(checkWSID == 2){
				mdlErrorSchema.ErrorCode = "06";
				mdlMessage.Indonesian = "Pengecekan WSID gagal";
				mdlMessage.English = mdlLog.ErrorMessage = "WSID check failed";
				mdlErrorSchema.ErrorMessage = mdlMessage;
				mdlUpdateDeviceManagementResult.ErrorSchema = mdlErrorSchema;
				mdlUpdateDeviceManagementResult.OutputSchema = mdlResult;
				LogAdapter.InsertLog(mdlLog);
				return mdlUpdateDeviceManagementResult;
			}

			checkDeviceManagement = DeviceManagementAdapter.CheckDeviceManagement(param.SerialNumber, param.WSID);
			//isSuccess = (checkDeviceManagement) ? DeviceManagementAdapter.UpdateDeviceManagement(param) : DeviceManagementAdapter.InsertDeviceManagement(param);
			if(checkDeviceManagement){
				//if serial number exists, then update existing data
				isSuccess = DeviceManagementAdapter.UpdateDeviceManagement(param);
			}else{
				//if serial number not exists, insert new data
				isSuccess = DeviceManagementAdapter.InsertDeviceManagement(param);
			}

			if (isSuccess){
			    	if (previousDeviceData != null && previousDeviceData.WSID != null && !previousDeviceData.WSID.isEmpty()){
			    	mdlLog.ErrorMessage = "Old Data = WSID :" + previousDeviceData.WSID + 
	    							", BranchCode : " + previousDeviceData.BranchCode + ""
	    							+ ", BranchTypeID : " + previousDeviceData.BranchTypeID + 
	    							", BranchInitial : " + previousDeviceData.BranchInitial + 
	    					      ". New Data = WSID :" + param.WSID + 
	    							", BranchCode : " + param.BranchCode + ""
	    							+ ", BranchTypeID : " + param.BranchTypeID + 
	    							", BranchInitial : " + param.BranchInitial;
			    	    
			    	}
			    	mdlLog.LogStatus = "Success";
				mdlErrorSchema.ErrorCode = "00";
				mdlMessage.Indonesian = "Data Berhasil Disimpan";
				mdlMessage.English = "Data Successfully Saved";
				mdlErrorSchema.ErrorMessage = mdlMessage;
				mdlUpdateDeviceManagementResult.ErrorSchema = mdlErrorSchema;
				mdlResult.Result = isSuccess.toString();
				mdlUpdateDeviceManagementResult.OutputSchema = mdlResult;
				
			}
			else{
				mdlErrorSchema.ErrorCode = "02";
				mdlMessage.Indonesian = "Data Gagal Disimpan";
				mdlMessage.English = mdlLog.ErrorMessage = "Save Data Failed";
				mdlErrorSchema.ErrorMessage = mdlMessage;
				mdlUpdateDeviceManagementResult.ErrorSchema = mdlErrorSchema;
				mdlUpdateDeviceManagementResult.OutputSchema = mdlResult;
			}
			String jsonIn = gson.toJson(param);
			logger.info("SUCCESS. API : BCAUpdateDevice, method : POST, jsonIn:" + jsonIn);
		}catch(Exception ex){
			mdlErrorSchema.ErrorCode = "03";
			mdlMessage.Indonesian = "Gagal memanggil service";
			mdlMessage.English = mdlLog.ErrorMessage = "Service call failed";
			mdlErrorSchema.ErrorMessage = mdlMessage;
			mdlUpdateDeviceManagementResult.ErrorSchema = mdlErrorSchema;
			mdlUpdateDeviceManagementResult.OutputSchema = mdlResult;
			String jsonIn = gson.toJson(param);
			logger.error("FAILED. API : BCAUpdateDevice, method : POST, jsonIn:" + jsonIn + ", Exception : " + ex.toString());
		}
		LogAdapter.InsertLog(mdlLog);
		return mdlUpdateDeviceManagementResult;
	}

}
