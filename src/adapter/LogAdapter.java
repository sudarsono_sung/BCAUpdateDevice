package adapter;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.time.LocalDateTime;

import org.apache.log4j.Logger;

import adapter.ConvertDateTimeHelper;
import model.Globals;

/** Documentation
 *
 */
public class LogAdapter {
	final static Logger logger = Logger.getLogger(LogAdapter.class);

	public static void InsertLog(model.mdlLog logModel)
	{
		Connection connection = null;
		PreparedStatement pstm = null;
		ResultSet jrs = null;
		try
		{
			String newLogID = CreateLogID();
			//define connection
			connection = database.RowSetAdapter.getConnectionWL();
			String sql = "INSERT INTO APILog (LogID, LogDate, LogSource, SerialNumber, WSID, APIFunction, SystemFunction, LogStatus, ErrorMessage)"
						+ "VALUES (?,TO_TIMESTAMP(?,'YYYY-MM-DD HH24:MI:SS.FF'),?,?,?,?,?,?,?)";

			pstm = connection.prepareStatement(sql);

			//insert sql parameter
			String dateNow = LocalDateTime.now().toString().replace("T", " ");
			pstm.setString(1, newLogID);
			pstm.setString(2, dateNow);
			pstm.setString(3, logModel.LogSource);
			pstm.setString(4, logModel.SerialNumber);
			pstm.setString(5, logModel.WSID);
			pstm.setString(6, logModel.ApiFunction);
			pstm.setString(7, logModel.SystemFunction);
			pstm.setString(8, logModel.LogStatus);
			pstm.setString(9, logModel.ErrorMessage);

			//execute query
			jrs = pstm.executeQuery();
		}
		catch(Exception ex) {
			logger.error("FAILED. API : BCAUpdateDevice, Function : InsertLog, Exception : " + ex.toString(), ex);
			System.out.print(ex.toString());
		}
		finally {
			try {
				//close the opened connection
				if (pstm != null) pstm.close();
				if (connection != null) connection.close();
				if (jrs != null) jrs.close();
			}
			catch(Exception e) {
				logger.error("FAILED. API : BCAUpdateDevice, Function : Close Connection InsertLog, Exception : " + e.toString(), e);
				Globals.gReturn_Status = "Insert log failed";
			}
		}
		return;
	}

	public static String CreateLogID(){
		String dateNow = LocalDateTime.now().toString().substring(0, 10).toString();
		String datetime = ConvertDateTimeHelper.formatDate(dateNow, "yyyy-MM-dd", "yyyyMMdd");
		String stringInc = "00000001";
		String lastLogID = getLastLogID(datetime);

		if ((lastLogID != null) && (!lastLogID.equals("")) ){
			String[] partsLogID = lastLogID.split("-");
			String partDate = partsLogID[1];
			String partNumber = partsLogID[2];

			//check if the date is same, if same, add 1 to the number
			if (partDate.equals(datetime)){
				int inc = Integer.parseInt(partNumber) + 1;
				stringInc = String.format("%08d", inc);
			}
		}
		StringBuilder sb = new StringBuilder();
		sb.append("API-").append(datetime).append("-").append(stringInc);
		String LogID = sb.toString();
		return LogID;
	}

	public static String getLastLogID(String currentDate){
		Connection connection = null;
		PreparedStatement pstm = null;
		ResultSet jrs = null;
		String LogID = "";
		try{
			connection = database.RowSetAdapter.getConnectionWL();
			pstm = connection.prepareStatement("SELECT NVL(MAX(LogID),'') as LogID FROM APILog WHERE SUBSTR(LogID,5,8) = ? ");
			pstm.setString(1, currentDate);

			jrs = pstm.executeQuery();

			while(jrs.next())
			{
				LogID = jrs.getString("LogID");
			}
		}
		catch (Exception ex){
			logger.error("FAILED. API : BCAUpdateDevice, Function : getLastLogID, Exception : " + ex.toString(), ex);
		}
		finally{
			//close the opened connection
			try{
				//close the opened connection
				if (pstm != null) pstm.close();
				if (connection != null) connection.close();
				if (jrs != null) jrs.close();
			}
			catch(Exception ex){
				logger.error("FAILED. API : BCAUpdateDevice, Function : Close Connection getLastLogID, Exception : " + ex.toString(), ex);
			}
		}
		return LogID;
	}

}
