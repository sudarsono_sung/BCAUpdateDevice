package adapter;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.apache.log4j.Logger;

public class DeviceManagementAdapter {
	final static Logger logger = Logger.getLogger(DeviceManagementAdapter.class);

	public static Integer CheckWSID(String SerialNumber, String WSID){
		model.mdlLog mdlLog = new model.mdlLog();
		mdlLog.ApiFunction = "updateDeviceManagement";
		mdlLog.SystemFunction = "CheckWSID";
		mdlLog.LogSource = "Webservice";
		mdlLog.SerialNumber = SerialNumber;
		mdlLog.WSID = WSID;
		mdlLog.LogStatus = "Failed";
		mdlLog.ErrorMessage = "";

		Connection connection = null;
		PreparedStatement pstm = null;
		ResultSet jrs = null;
		Integer returnValue = 0;
		try{
			//define connection
			connection = database.RowSetAdapter.getConnectionWL();
			String sql = "SELECT SerialNumber FROM ms_device WHERE WSID = ?";
			pstm = connection.prepareStatement(sql);
			//insert sql parameter
			pstm.setString(1, WSID);
			//execute query
			jrs = pstm.executeQuery();
			while(jrs.next()){
				String SerialNumberFromDB = jrs.getString("SerialNumber");
				//if serial number is not same, then return error
				if (!SerialNumber.equalsIgnoreCase(SerialNumberFromDB)){
					returnValue = 1;
				}
			}
			mdlLog.LogStatus = "Success";
		}
		catch(Exception ex) {
			returnValue = 2;
			logger.error("FAILED. API : BCAUpdateDevice, method : POST, function: CheckWSID, Exception : " + ex.toString(), ex);
			mdlLog.ErrorMessage = ex.toString();
			LogAdapter.InsertLog(mdlLog);
		}
		finally {
			try {
				//close the opened connection
				 if (pstm != null) pstm.close();
				 if (connection != null) connection.close();
				 if (jrs != null) jrs.close();
			}
			catch(Exception e) {

			}
		}
		return returnValue;
	}

	public static Boolean UpdateDeviceManagement(model.mdlDeviceManagement mdlDeviceManagement){
		model.mdlLog mdlLog = new model.mdlLog();
		mdlLog.ApiFunction = "updateDeviceManagement";
		mdlLog.SystemFunction = "UpdateDeviceManagement";
		mdlLog.LogSource = "Webservice";
		mdlLog.SerialNumber = mdlDeviceManagement.SerialNumber;
		mdlLog.WSID = mdlDeviceManagement.WSID;
		mdlLog.LogStatus = "Failed";
		mdlLog.ErrorMessage = "";

		Connection connection = null;
		PreparedStatement pstm = null;
		ResultSet jrs = null;
		boolean isSuccess = false;
		//check if device is exists or not
		try{
			//define connection
			connection = database.RowSetAdapter.getConnectionWL();
			//call store procedure
			String sql = "UPDATE ms_device SET WSID = ?, BranchCode = ?, BranchTypeID = ?, UpdateDate = TO_TIMESTAMP(?, 'YYYY-MM-DD HH24:MI:SS.FF'), "
						+ "UpdateBy = ?, BranchInitial = ? WHERE SerialNumber = ?";
			pstm = connection.prepareStatement(sql);

			//insert sql parameter
			pstm.setString(1, mdlDeviceManagement.WSID);
			pstm.setString(2, mdlDeviceManagement.BranchCode);
			pstm.setString(3, mdlDeviceManagement.BranchTypeID);
			pstm.setString(4, mdlDeviceManagement.UpdateDate);
			pstm.setString(5, mdlDeviceManagement.UpdateBy);
			pstm.setString(6, mdlDeviceManagement.BranchInitial);
			pstm.setString(7, mdlDeviceManagement.SerialNumber);

			//execute query
			jrs = pstm.executeQuery();
			isSuccess = true;
			mdlLog.LogStatus = "Success";
		}
		catch(Exception ex) {
			logger.error("FAILED. API : BCAUpdateDevice, method : POST, function: UpdateDeviceManagement, Exception : " + ex.toString(), ex);
			mdlLog.ErrorMessage = ex.toString();
			LogAdapter.InsertLog(mdlLog);
		}
		finally {
			try {
				//close the opened connection
				 if (pstm != null) pstm.close();
				 if (connection != null) connection.close();
				 if (jrs != null) jrs.close();
			}
			catch(Exception e) {

			}
		}
		return isSuccess;
	}

	public static boolean CheckDeviceManagement(String SerialNumber, String WSID){
		model.mdlLog mdlLog = new model.mdlLog();
		mdlLog.ApiFunction = "updateDeviceManagement";
		mdlLog.SystemFunction = "CheckDeviceManagement";
		mdlLog.LogSource = "Webservice";
		mdlLog.SerialNumber = SerialNumber;
		mdlLog.WSID = WSID;
		mdlLog.LogStatus = "Failed";
		mdlLog.ErrorMessage = "";

		Connection connection = null;
		PreparedStatement pstm = null;
		ResultSet jrs = null;
		boolean isExists = false;
		//check if device is exists or not
		try{
			//define connection
			connection = database.RowSetAdapter.getConnectionWL();

			//call store procedure
			String sql = "SELECT SerialNumber FROM ms_device WHERE SerialNumber = ? FETCH FIRST 1 ROWS ONLY";
			pstm = connection.prepareStatement(sql);
			//insert sql parameter
			pstm.setString(1, SerialNumber);
			//execute query
			jrs = pstm.executeQuery();
			while(jrs.next()){
				isExists = true;
			}
			mdlLog.LogStatus = "Success";
		}
		catch(Exception ex) {
			logger.error("FAILED. API : BCAUpdateDevice, method : POST, function: CheckDeviceManagement, Exception : " + ex.toString(), ex);
			mdlLog.ErrorMessage = ex.toString();
			LogAdapter.InsertLog(mdlLog);
		}
		finally {
			try {
				//close the opened connection
				 if (pstm != null) pstm.close();
				 if (connection != null) connection.close();
				 if (jrs != null) jrs.close();
			}
			catch(Exception e) {
			    logger.error("FAILED. API : BCAUpdateDevice, Function : Close Connection CheckDeviceManagement, Exception : " + e.toString(), e);
			}
		}
		return isExists;
	}

	public static Boolean InsertDeviceManagement(model.mdlDeviceManagement mdlDeviceManagement){
		model.mdlLog mdlLog = new model.mdlLog();
		mdlLog.ApiFunction = "updateDeviceManagement";
		mdlLog.SystemFunction = "InsertDeviceManagement";
		mdlLog.LogSource = "Webservice";
		mdlLog.SerialNumber = mdlDeviceManagement.SerialNumber;
		mdlLog.WSID = mdlDeviceManagement.WSID;
		mdlLog.LogStatus = "Failed";
		mdlLog.ErrorMessage = "";

		Connection connection = null;
		PreparedStatement pstm = null;
		ResultSet jrs = null;
		boolean isSuccess = false;
		//check if device is exists or not
		try{
			//define connection
			connection = database.RowSetAdapter.getConnectionWL();
			//call store procedure
			String sql = "INSERT INTO ms_device (SerialNumber,WSID,BranchCode,BranchTypeID,BranchInitial,UpdateDate,UpdateBy) "
						+ "VALUES (?,?,?,?,?,TO_TIMESTAMP(?, 'YYYY-MM-DD HH24:MI:SS.FF'),?)";
			pstm = connection.prepareStatement(sql);

			//insert sql parameter
			pstm.setString(1, mdlDeviceManagement.SerialNumber);
			pstm.setString(2, mdlDeviceManagement.WSID);
			pstm.setString(3, mdlDeviceManagement.BranchCode);
			pstm.setString(4, mdlDeviceManagement.BranchTypeID);
			pstm.setString(5, mdlDeviceManagement.BranchInitial);
			pstm.setString(6, mdlDeviceManagement.UpdateDate);
			pstm.setString(7, mdlDeviceManagement.UpdateBy);

			//execute query
			jrs = pstm.executeQuery();
			isSuccess = true;
			mdlLog.LogStatus = "Success";
		}
		catch(Exception ex) {
			logger.error("FAILED. API : BCAUpdateDevice, method : POST, function: InsertDeviceManagement, Exception : " + ex.toString(), ex);
			mdlLog.ErrorMessage = ex.toString();
			LogAdapter.InsertLog(mdlLog);
		}
		finally {
			try {
				//close the opened connection
				 if (pstm != null) pstm.close();
				 if (connection != null) connection.close();
				 if (jrs != null) jrs.close();
			}
			catch(Exception e) {
			    logger.error("FAILED. API : BCAUpdateDevice, Function : Close Connection InsertDeviceManagement, Exception : " + e.toString(), e);
			}
		}
		return isSuccess;
	}

	public static boolean CheckBranchCode(String BranchCode, String BranchTypeID, String BranchInitial, String SerialNumber, String WSID){
		model.mdlLog mdlLog = new model.mdlLog();
		mdlLog.ApiFunction = "updateDeviceManagement";
		mdlLog.SystemFunction = "CheckBranchCode";
		mdlLog.LogSource = "Webservice";
		mdlLog.SerialNumber = SerialNumber;
		mdlLog.WSID = WSID;
		mdlLog.LogStatus = "Failed";
		mdlLog.ErrorMessage = "";

		Connection connection = null;
		PreparedStatement pstm = null;
		ResultSet jrs = null;
		boolean isExists = false;
		try{
			//define connection
			connection = database.RowSetAdapter.getConnectionWL();

			//call store procedure
			String sql = "SELECT BranchCode FROM ms_branch WHERE BranchCode = ? AND BranchTypeID = ? AND BranchInitial = ?";
			pstm = connection.prepareStatement(sql);
			//insert sql parameter
			pstm.setString(1, BranchCode);
			pstm.setString(2, BranchTypeID);
			pstm.setString(3, BranchInitial);
			//execute query
			jrs = pstm.executeQuery();
			while(jrs.next()){
				isExists = true;
			}
			mdlLog.LogStatus = "Success";
		}
		catch(Exception ex) {
			logger.error("FAILED. API : BCAUpdateDevice, method : POST, function: CheckBranchCode, Exception : " + ex.toString(), ex);
			mdlLog.ErrorMessage = ex.toString();
			LogAdapter.InsertLog(mdlLog);
		}
		finally {
			try {
				//close the opened connection
				 if (pstm != null) pstm.close();
				 if (connection != null) connection.close();
				 if (jrs != null) jrs.close();
			}
			catch(Exception e) {
			    logger.error("FAILED. API : BCAUpdateDevice, Function : Close Connection CheckBranchCode, Exception : " + e.toString(), e);
			}
		}
		return isExists;
	}

	public static boolean CheckWSIDBranchCode(String BranchCode, String WSID, String SerialNumber){
		model.mdlLog mdlLog = new model.mdlLog();
		mdlLog.ApiFunction = "updateDeviceManagement";
		mdlLog.SystemFunction = "CheckWSIDBranchCode";
		mdlLog.LogSource = "Webservice";
		mdlLog.SerialNumber = SerialNumber;
		mdlLog.WSID = WSID;
		mdlLog.LogStatus = "Failed";
		mdlLog.ErrorMessage = "";

		boolean isMatch = false;
		try{

			if(BranchCode.contentEquals(WSID.substring(0, 4))){
				mdlLog.LogStatus = "Success";
				isMatch = true;
			}
		}
		catch(Exception ex) {
			logger.error("FAILED. API : BCAUpdateDevice, method : POST, function: CheckWSIDBranchCode, Exception : " + ex.toString(), ex);
			mdlLog.ErrorMessage = ex.toString();
			LogAdapter.InsertLog(mdlLog);
		}
		finally {

		}

		return isMatch;
	}
	
	public static model.mdlDeviceManagement GetDeviceData(String SerialNumber, String WSID){
	    model.mdlDeviceManagement deviceData = new model.mdlDeviceManagement();
	    model.mdlLog mdlLog = new model.mdlLog();
	    mdlLog.ApiFunction = "updateDeviceManagement";
		mdlLog.SystemFunction = "GetDeviceData";
		mdlLog.LogSource = "Webservice";
		mdlLog.SerialNumber = SerialNumber;
		mdlLog.WSID = WSID;
		mdlLog.LogStatus = "Failed";
		mdlLog.ErrorMessage = "";

		Connection connection = null;
		PreparedStatement pstm = null;
		ResultSet jrs = null;
		try{
			//define connection
			connection = database.RowSetAdapter.getConnectionWL();

			//call store procedure
			String sql = "SELECT WSID,BranchCode,BranchTypeID,BranchInitial FROM ms_device WHERE SerialNumber = ? ";
			pstm = connection.prepareStatement(sql);
			//insert sql parameter
			pstm.setString(1, SerialNumber);
			//execute query
			jrs = pstm.executeQuery();
			while(jrs.next()){
			    deviceData.SerialNumber = SerialNumber;
			    deviceData.WSID = jrs.getString("WSID");
			    deviceData.BranchCode = jrs.getString("BranchCode");
			    deviceData.BranchInitial = jrs.getString("BranchInitial");
			    deviceData.BranchTypeID = jrs.getString("BranchTypeID");
			}
			mdlLog.LogStatus = "Success";
		}
		catch(Exception ex) {
			logger.error("FAILED. API : BCAUpdateDevice, function: GetDeviceData, Exception : " + ex.toString(), ex);
			mdlLog.ErrorMessage = ex.toString();
			LogAdapter.InsertLog(mdlLog);
		}
		finally {
			try {
				//close the opened connection
				 if (pstm != null) pstm.close();
				 if (connection != null) connection.close();
				 if (jrs != null) jrs.close();
			}
			catch(Exception e) {
			    logger.error("FAILED. API : BCAUpdateDevice, Function : Close Connection GetDeviceData, Exception : " + e.toString(), e);
			}
		}
	    
	    return deviceData;
	}

}